/*
 *   Copyright 2019-2020 Magnus Kronnäs, se LICENCE.txt
 *
 */
"use strict";

import { Logger } from "./base";

import yargs from "yargs";

import moment from "moment";

import useragent from "useragent";
import fs from "fs-extra";
import path from "path";
import schedule from "node-schedule";

import { t, Text, TextToTerminal, TextToString } from "@mimer/text";

// Advise from https://www.npmjs.com/package/sleep
function msleep(n: number) {
  Atomics.wait(new Int32Array(new SharedArrayBuffer(4)), 0, 0, n);
}

function sleep(n: number) {
  msleep(n * 1000);
}

const pause = 3;

export abstract class LoggerTerminal extends Logger {
  private static _logPath = "";

  /**
   * Sets a yarg argument
   *
   * @param yarg A yarg object
   */
  static yarg(yarg: any) {
    return yarg
      .describe("v", "Give a verbose print")
      .alias("v", "verbose")
      .default("v", false)
      .describe("d", "Give a debug print")
      .alias("d", "debug")
      .default("d", false)
      .describe("t", "Give a trace print")
      .alias("t", "trace")
      .default("t", false)
      .help("h")
      .alias("h", "help")
      .boolean(["d", "v", "t", "h"]);
  }

  static logFile(logDir: string) {
    this._logPath = path.resolve(logDir);
    return this;
  }

  static log = function() {
    Logger._toText = (x: Text) => TextToTerminal(x);
    Logger.log();

    //Bad solution, better to give standar log and then have a view program if needed.
    if (Logger._level > 3) {
      LoggerTerminal.web = function(req: any) {
        const agent = useragent.parse(req.headers["user-agent"]);
        let log_text = t(
          t("[", moment().format("YYYY-MM-DD HH:mm:ss"), "] ").green,
          t(req.ip).bold,
          " ",
          t(req.protocol).red,
          " ",
          t(req.method).green,
          " ",
          t(req.originalUrl).yellow,
          " ",
          t(agent.toAgent()).green,
          " ",
          t(agent.os.toString()).bold,
          " ",
          t(agent.device.toString()).yellow
        );
        console.log(TextToTerminal(log_text));

        if (LoggerTerminal._logPath) {
          fs.appendFile(
            LoggerTerminal._logPath,
            TextToString(log_text.newline)
          );
        }
      };
    } else {
      LoggerTerminal.web = (x: any): void => {};
    }

    if (Logger._level > 3) {
      LoggerTerminal.confirm = function() {
        console.log(
          TextToTerminal(t("\nIs this rigth? Ctrl-C to cancel.").yellow.bold)
        );
        sleep(pause);
      };
    } else {
      LoggerTerminal.confirm = (): void => {};
    }
  };
}
