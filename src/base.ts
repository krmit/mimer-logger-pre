/*
 *   Copyright 2019-2020 Magnus Kronnäs, se LICENCE.txt
 *
 */
"use strict";

import { t, Text, TextToString } from "@mimer/text";

// Some problem has been observed with jsome, but no cause is knowe. Shoule replace it with a function in Text
import { getColoredString as pretty } from "jsome";
//const pretty = function(obj){return JSON.stringify(obj, null, 2)};

/**
 * The Logger class that can create a log object.
 */
export abstract class Logger {
  protected static _level = 5;
  protected static _toText: (x: Text) => string = (x: Text) => TextToString(x);

  static trace: (x: any) => void = (x: any): void => {};
  static debug: (x: any) => void = (x: any): void => {};
  static warn: (x: any) => void = (x: any): void => {};
  static info: (x: any) => void = (x: any): void => {};
  static error: (x: any) => void = (x: any): void => {};
  static fatal: (x: any) => void = (x: any): void => {};
  static show: (x: string | Text) => void = (x: string | Text): void => {};
  static print: (x: any) => void = (x: any): void => {};
  static web: (x: any) => void = (x: any): void => {};
  static confirm: () => void = (): void => {};

  /**
   * Create a Logger
   *
   */
  constructor() {
    return this;
  }

  /*
   *
   */

  static parseArg(arg: any): void {
    if (arg.verbose) {
      Logger._level = 5;
    } else if (arg.debug) {
      Logger._level = 6;
    } else if (arg.trace) {
      Logger._level = 7;
    }
  }

  static level(level: number = 5) {
    Logger._level = level;
  }

  private static logPrint = function(
    tag: string,
    tagColor = (x: string) => new Text(x),
    consoleMethod = (x: Text) => console.log(Logger._toText(x)),
    msgColor = (x: string | Text) => new Text(x)
  ) {
    return (msg: any) => {
      if (typeof msg === "string" || msg instanceof Text) {
        consoleMethod(
          t(
            t("[", tagColor(tag), "]"),
            t(" ").span(6 - tag.length),
            msgColor(msg)
          )
        );
      } else if (typeof msg === "boolean" || typeof msg === "number") {
        consoleMethod(
          t(
            t("[", tagColor(tag), "]"),
            t(" ").span(6 - tag.length),
            msgColor(String(msg))
          )
        );
      } else {
        // Needed good pretty printing of json and object because of circular objects.
        consoleMethod(t(t("[", tagColor(tag), "]").newline));
        console.log(pretty(msg)); // Shoul be console_metod(msg) if pretty could give a Text object.
      }
    };
  };

  static log = function() {
    if (Logger._level > 6) {
      Logger.trace = Logger.logPrint(
        "trace",
        (x: string) => new Text(x).green,
        (x: Text) => console.debug(Logger._toText(x))
      );
    } else {
      Logger.trace = (x: any): void => {};
    }

    if (Logger._level > 5) {
      Logger.debug = Logger.logPrint(
        "debug",
        (x: string) => new Text(x).green,
        (x: Text) => console.debug(Logger._toText(x))
      );
    } else {
      Logger.debug = (x: any): void => {};
    }

    if (Logger._level > 4) {
      Logger.info = Logger.logPrint(
        "info",
        (x: string) => new Text(x).green,
        (x: Text) => console.info(Logger._toText(x))
      );
    } else {
      Logger.info = (x: any): void => {};
    }

    if (Logger._level > 3) {
      Logger.warn = Logger.logPrint(
        "warn",
        (x: string) => new Text(x).yellow,
        (x: Text) => console.warn(Logger._toText(x))
      );
    } else {
      Logger.warn = (x: any): void => {};
    }

    if (Logger._level > 2) {
      Logger.error = Logger.logPrint(
        "error",
        (x: string) => new Text(x).red,
        (x: Text) => console.error(Logger._toText(x))
      );
    } else {
      Logger.error = (x: any): void => {};
    }

    if (Logger._level > 1) {
      Logger.fatal = Logger.logPrint(
        "fatal",
        (x: string) => new Text(x).red,
        (x: Text) => console.error(Logger._toText(x)),
        (x: string | Text) => new Text(x).bold.red
      );
    } else {
      Logger.fatal = (x: any): void => {};
    }

    if (Logger._level > 3) {
      Logger.print = (x: any): void => console.log(x);
    } else {
      Logger.print = (x: any): void => {};
    }

    if (Logger._level > 3) {
      Logger.show = (x: string | Text) =>
        console.log(Logger._toText(new Text(x)));
    } else {
      Logger.show = (x: string | Text): void => {};
    }

    if (Logger._level > 3) {
      Logger.print = (x: any): void => console.log(x);
    } else {
      Logger.print = (x: any): void => {};
    }
  };
}
