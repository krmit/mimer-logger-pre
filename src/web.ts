/*
 *   Copyright 2019-2020 Magnus Kronnäs, se LICENCE.txt
 *
 */
"use strict";

import { Logger } from "./base";

export abstract class LoggerWeb extends Logger {}
