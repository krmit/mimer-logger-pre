#!/usr/bin/node
"use strict";
import { LoggerTerminal as log } from "../src";
import { t, Text } from "@mimer/text";

log.level(7);
log.log();

log.print("Test!!");

log.fatal("Very very bad!");
log.error("Very bad!");
log.warn("Could be bad!");
log.info("For your information!");
log.debug("Something is wrong!");
log.trace("Where is the bug?!");

log.debug(true);
log.info(42);
log.info(["This", "is", "a", "test"]);
log.info({ data: [1, 2, 3], id: 1 });

let req = {
  ip: "10.10.10.10",
  protocol: "https",
  method: "post",
  originalUrl: "https://www.dn.se",
  headers: {
    "user-agent":
      "Mozilla/5.0 (iPad; U; CPU OS 3_2_1 like Mac OS X; en-us) AppleWebKit/531.21.10 (KHTML, like Gecko) Mobile/7B405"
  }
};

log.print(req.headers["user-agent"]);

log.show(t("1+1===2").important);
log.show(t("I am a headline!").headline);
log.confirm();
log.show(t("And I am important!!").important);
log.web(req);
log.show("Finaly");
log.trace("By!");
