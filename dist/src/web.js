/*
 *   Copyright 2019-2020 Magnus Kronnäs, se LICENCE.txt
 *
 */
"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.LoggerWeb = void 0;
const base_1 = require("./base");
class LoggerWeb extends base_1.Logger {
}
exports.LoggerWeb = LoggerWeb;
