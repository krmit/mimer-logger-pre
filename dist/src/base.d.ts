import { Text } from "@mimer/text";
/**
 * The Logger class that can create a log object.
 */
export declare abstract class Logger {
    protected static _level: number;
    protected static _toText: (x: Text) => string;
    static trace: (x: any) => void;
    static debug: (x: any) => void;
    static warn: (x: any) => void;
    static info: (x: any) => void;
    static error: (x: any) => void;
    static fatal: (x: any) => void;
    static show: (x: string | Text) => void;
    static print: (x: any) => void;
    static web: (x: any) => void;
    static confirm: () => void;
    /**
     * Create a Logger
     *
     */
    constructor();
    static parseArg(arg: any): void;
    static level(level?: number): void;
    private static logPrint;
    static log: () => void;
}
