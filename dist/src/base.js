/*
 *   Copyright 2019-2020 Magnus Kronnäs, se LICENCE.txt
 *
 */
"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.Logger = void 0;
const text_1 = require("@mimer/text");
// Some problem has been observed with jsome, but no cause is knowe. Shoule replace it with a function in Text
const jsome_1 = require("jsome");
//const pretty = function(obj){return JSON.stringify(obj, null, 2)};
/**
 * The Logger class that can create a log object.
 */
class Logger {
    /**
     * Create a Logger
     *
     */
    constructor() {
        return this;
    }
    /*
     *
     */
    static parseArg(arg) {
        if (arg.verbose) {
            Logger._level = 5;
        }
        else if (arg.debug) {
            Logger._level = 6;
        }
        else if (arg.trace) {
            Logger._level = 7;
        }
    }
    static level(level = 5) {
        Logger._level = level;
    }
}
exports.Logger = Logger;
Logger._level = 5;
Logger._toText = (x) => text_1.TextToString(x);
Logger.trace = (x) => { };
Logger.debug = (x) => { };
Logger.warn = (x) => { };
Logger.info = (x) => { };
Logger.error = (x) => { };
Logger.fatal = (x) => { };
Logger.show = (x) => { };
Logger.print = (x) => { };
Logger.web = (x) => { };
Logger.confirm = () => { };
Logger.logPrint = function (tag, tagColor = (x) => new text_1.Text(x), consoleMethod = (x) => console.log(Logger._toText(x)), msgColor = (x) => new text_1.Text(x)) {
    return (msg) => {
        if (typeof msg === "string" || msg instanceof text_1.Text) {
            consoleMethod(text_1.t(text_1.t("[", tagColor(tag), "]"), text_1.t(" ").span(6 - tag.length), msgColor(msg)));
        }
        else if (typeof msg === "boolean" || typeof msg === "number") {
            consoleMethod(text_1.t(text_1.t("[", tagColor(tag), "]"), text_1.t(" ").span(6 - tag.length), msgColor(String(msg))));
        }
        else {
            // Needed good pretty printing of json and object because of circular objects.
            consoleMethod(text_1.t(text_1.t("[", tagColor(tag), "]").newline));
            console.log(jsome_1.getColoredString(msg)); // Shoul be console_metod(msg) if pretty could give a Text object.
        }
    };
};
Logger.log = function () {
    if (Logger._level > 6) {
        Logger.trace = Logger.logPrint("trace", (x) => new text_1.Text(x).green, (x) => console.debug(Logger._toText(x)));
    }
    else {
        Logger.trace = (x) => { };
    }
    if (Logger._level > 5) {
        Logger.debug = Logger.logPrint("debug", (x) => new text_1.Text(x).green, (x) => console.debug(Logger._toText(x)));
    }
    else {
        Logger.debug = (x) => { };
    }
    if (Logger._level > 4) {
        Logger.info = Logger.logPrint("info", (x) => new text_1.Text(x).green, (x) => console.info(Logger._toText(x)));
    }
    else {
        Logger.info = (x) => { };
    }
    if (Logger._level > 3) {
        Logger.warn = Logger.logPrint("warn", (x) => new text_1.Text(x).yellow, (x) => console.warn(Logger._toText(x)));
    }
    else {
        Logger.warn = (x) => { };
    }
    if (Logger._level > 2) {
        Logger.error = Logger.logPrint("error", (x) => new text_1.Text(x).red, (x) => console.error(Logger._toText(x)));
    }
    else {
        Logger.error = (x) => { };
    }
    if (Logger._level > 1) {
        Logger.fatal = Logger.logPrint("fatal", (x) => new text_1.Text(x).red, (x) => console.error(Logger._toText(x)), (x) => new text_1.Text(x).bold.red);
    }
    else {
        Logger.fatal = (x) => { };
    }
    if (Logger._level > 3) {
        Logger.print = (x) => console.log(x);
    }
    else {
        Logger.print = (x) => { };
    }
    if (Logger._level > 3) {
        Logger.show = (x) => console.log(Logger._toText(new text_1.Text(x)));
    }
    else {
        Logger.show = (x) => { };
    }
    if (Logger._level > 3) {
        Logger.print = (x) => console.log(x);
    }
    else {
        Logger.print = (x) => { };
    }
};
