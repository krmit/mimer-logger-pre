/*
 *   Copyright 2019-2020 Magnus Kronnäs, se LICENCE.txt
 *
 */
"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.LoggerTerminal = void 0;
const base_1 = require("./base");
const moment_1 = __importDefault(require("moment"));
const useragent_1 = __importDefault(require("useragent"));
const fs_extra_1 = __importDefault(require("fs-extra"));
const path_1 = __importDefault(require("path"));
const text_1 = require("@mimer/text");
// Advise from https://www.npmjs.com/package/sleep
function msleep(n) {
    Atomics.wait(new Int32Array(new SharedArrayBuffer(4)), 0, 0, n);
}
function sleep(n) {
    msleep(n * 1000);
}
const pause = 3;
class LoggerTerminal extends base_1.Logger {
    /**
     * Sets a yarg argument
     *
     * @param yarg A yarg object
     */
    static yarg(yarg) {
        return yarg
            .describe("v", "Give a verbose print")
            .alias("v", "verbose")
            .default("v", false)
            .describe("d", "Give a debug print")
            .alias("d", "debug")
            .default("d", false)
            .describe("t", "Give a trace print")
            .alias("t", "trace")
            .default("t", false)
            .help("h")
            .alias("h", "help")
            .boolean(["d", "v", "t", "h"]);
    }
    static logFile(logDir) {
        this._logPath = path_1.default.resolve(logDir);
        return this;
    }
}
exports.LoggerTerminal = LoggerTerminal;
LoggerTerminal._logPath = "";
LoggerTerminal.log = function () {
    base_1.Logger._toText = (x) => text_1.TextToTerminal(x);
    base_1.Logger.log();
    //Bad solution, better to give standar log and then have a view program if needed.
    if (base_1.Logger._level > 3) {
        LoggerTerminal.web = function (req) {
            const agent = useragent_1.default.parse(req.headers["user-agent"]);
            let log_text = text_1.t(text_1.t("[", moment_1.default().format("YYYY-MM-DD HH:mm:ss"), "] ").green, text_1.t(req.ip).bold, " ", text_1.t(req.protocol).red, " ", text_1.t(req.method).green, " ", text_1.t(req.originalUrl).yellow, " ", text_1.t(agent.toAgent()).green, " ", text_1.t(agent.os.toString()).bold, " ", text_1.t(agent.device.toString()).yellow);
            console.log(text_1.TextToTerminal(log_text));
            if (LoggerTerminal._logPath) {
                fs_extra_1.default.appendFile(LoggerTerminal._logPath, text_1.TextToString(log_text.newline));
            }
        };
    }
    else {
        LoggerTerminal.web = (x) => { };
    }
    if (base_1.Logger._level > 3) {
        LoggerTerminal.confirm = function () {
            console.log(text_1.TextToTerminal(text_1.t("\nIs this rigth? Ctrl-C to cancel.").yellow.bold));
            sleep(pause);
        };
    }
    else {
        LoggerTerminal.confirm = () => { };
    }
};
