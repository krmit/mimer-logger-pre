import { Logger } from "./base";
export declare abstract class LoggerTerminal extends Logger {
    private static _logPath;
    /**
     * Sets a yarg argument
     *
     * @param yarg A yarg object
     */
    static yarg(yarg: any): any;
    static logFile(logDir: string): typeof LoggerTerminal;
    static log: () => void;
}
