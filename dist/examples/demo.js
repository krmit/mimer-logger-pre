#!/usr/bin/node
"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const src_1 = require("../src");
const text_1 = require("@mimer/text");
src_1.LoggerTerminal.level(7);
src_1.LoggerTerminal.log();
src_1.LoggerTerminal.print("Test!!");
src_1.LoggerTerminal.fatal("Very very bad!");
src_1.LoggerTerminal.error("Very bad!");
src_1.LoggerTerminal.warn("Could be bad!");
src_1.LoggerTerminal.info("For your information!");
src_1.LoggerTerminal.debug("Something is wrong!");
src_1.LoggerTerminal.trace("Where is the bug?!");
src_1.LoggerTerminal.debug(true);
src_1.LoggerTerminal.info(42);
src_1.LoggerTerminal.info(["This", "is", "a", "test"]);
src_1.LoggerTerminal.info({ data: [1, 2, 3], id: 1 });
let req = {
    ip: "10.10.10.10",
    protocol: "https",
    method: "post",
    originalUrl: "https://www.dn.se",
    headers: {
        "user-agent": "Mozilla/5.0 (iPad; U; CPU OS 3_2_1 like Mac OS X; en-us) AppleWebKit/531.21.10 (KHTML, like Gecko) Mobile/7B405"
    }
};
src_1.LoggerTerminal.print(req.headers["user-agent"]);
src_1.LoggerTerminal.show(text_1.t("1+1===2").important);
src_1.LoggerTerminal.show(text_1.t("I am a headline!").headline);
src_1.LoggerTerminal.confirm();
src_1.LoggerTerminal.show(text_1.t("And I am important!!").important);
src_1.LoggerTerminal.web(req);
src_1.LoggerTerminal.show("Finaly");
src_1.LoggerTerminal.trace("By!");
